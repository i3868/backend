const express = require('express')
const app = express()
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const routesUrls = require('./routes/routes')
const cors = require('cors')


dotenv.config()
//connect our app to mongoDB
mongoose.connect(process.env.DATABASE_ACCESS).then(
  () => { console.log("Database connected") },
  err => { console.log(err.message) }
);
//enable CORS  
app.use(cors())
//activate body parser
app.use(express.json())
app.use('/', routesUrls)
app.listen(1337, () => {
    console.log('Server is up and running')
})

const promClient = require('prom-client');
const collectDefaultMetrics = promClient.collectDefaultMetrics;
const register = new promClient.Registry();
register.setDefaultLabels({
    component: 'backend'
})
collectDefaultMetrics({ register });

const totalUsers = new promClient.Counter({
    name: 'total_users',
    help: 'The total number of users that have registered in app.',
});


const noEvents = new promClient.Gauge({ 
    name: 'no_events',
    help: 'The total number of the existing events in app.'
});

const noNews = new promClient.Gauge({ 
    name: 'no_news',
    help: 'The total number of news existing in app.'
});

const noEnrollments = new promClient.Gauge({ 
    name: 'no_enrollments',
    help: 'The total number of enrollments existing in app.'
});

register.registerMetric(totalUsers)
register.registerMetric(noEvents)
register.registerMetric(noNews)
register.registerMetric(noEnrollments)

app.get('/metrics', async (req, res) => {
    res.setHeader('Content-Type', register.contentType)
    res.end(await register.metrics())
})

exports.noNews = noNews
exports.noEvents = noEvents
exports.noEnrollments = noEnrollments
exports.totalUsers = totalUsers
