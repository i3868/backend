const express = require('express')
const router = express.Router()
const users = require('../controllers/userController')  
const events = require('../controllers/eventController')
const enrollments = require('../controllers/enrollmentControler')
const news = require('../controllers/newsController')
const AdminPermission = require('../middleware/adminPermissions')
const checkPermissions = require('../middleware/permissions')
const checkJwt = require('../middleware/jwtCheck')

    router.post("/users", users.createUser);   
    router.get("/users:id", users.findUser);  
    router.get("/events", events.findAllEvents);
    router.get("/events:id", events.findOneEvent);
    router.put("/events/participants:id/:value", events.updateParticipantsNumber);
    router.get("/events/location:location", events.findLocationEvents);
    router.post("/enrollments", enrollments.createEnrollment);
    router.get(`/enrollments:id`, enrollments.findUserEnrollment);
    router.delete(`/enrollments:id`, enrollments.deleteAllEnrollmentsEventBased);
    router.delete(`/enrollments/:eventId/:userId`, enrollments.deleteEnrollment);
    router.get("/news", news.findAllNews);
    /* ADMIN only
    router.use(checkJwt);
    router.post("/news", checkPermissions(AdminPermission.CreateNews), news.createPieceOfNews);
    router.put(`/news:id`, checkPermissions(AdminPermission.UpdateNews), news.updatePieceOfNews)
    router.put(`/events:id`, checkPermissions(AdminPermission.UpdateEvents), events.updateEvent);
    router.post("/events", checkPermissions(AdminPermission.CreateEvents), events.createEvent);
    router.delete("/events:id", checkPermissions(AdminPermission.DeleteEvents), events.deleteEvent);
    router.delete("/news:id", checkPermissions(AdminPermission.DeleteNews), news.deleteNews);*/
    router.post("/news", news.createPieceOfNews);
    router.put(`/news:id`, news.updatePieceOfNews)
    router.put(`/events:id`, events.updateEvent);
    router.post("/events",  events.createEvent);
    router.delete("/events:id",  events.deleteEvent);
    router.delete("/news:id", news.deleteNews)

module.exports = router
