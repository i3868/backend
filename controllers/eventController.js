const Event = require('../models/Event')
const metrics = require('../server')
// Create and Save a new event
exports.createEvent = async(req, res) => {
    let event = await Event.findOne({ name : req.body.name});
    if (event) {
      return res.status(400).send('Event with the provided name already exist.');
    }
    // Create an event
    event = new Event({
        name: req.body.name,
        eventDate: req.body.eventDate,
        eventLocation: (req.body.eventLocation).toUpperCase(),
        maxParticipants: req.body.maxParticipants,
        currentParticipantsNumber: req.body.currentParticipantsNumber,
        purpose:  req.body.purpose
    });
    // Save event in the database
    event
        .save(event)
        .then(data => {
            metrics.noEvents.inc()
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while creating the user."
            });
        });
};

// Retrieve all events from the database.
exports.findAllEvents = async (req, res) => {
    await Event.find({})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while retrieving events."
            });
        });
};

//Retrieve an event based on _id
exports.findOneEvent = async (req, res) => {
    const id = req.params.id
    await Event.find({_id: Object(id)})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while retrieving events."
            });
        });
};

exports.findLocationEvents = async(req, res) => {
    let location = (req.params.location).toUpperCase()
    console.log(location)
    await Event.find({ eventLocation : location})
        .then(data => {
            res.send(data);
        })
        
}

// Delete an event based on name
exports.deleteEvent = async (req, res) => {
    const {id} = req.params;

    await Event.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete event with id=${id}. Maybe event was not found!`
                });
            } else {
                metrics.noEvents.dec()
                res.send({
                    message: "Event was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete event with id=" + id
            });
        });
};   

exports.updateEvent = async(req, res) => {
  
    const filter = { _id: req.params.id };
    let update = {
        name: req.body.name,
        eventDate: req.body.eventDate,
        eventLocation: (req.body.eventLocation).toUpperCase(),
        maxParticipants: req.body.maxParticipants,
        currentParticipantsNumber: req.body.currentParticipantsNumber,
        purpose:  req.body.purpose
    };
      
    await Event.updateOne(filter, { $set: update })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update piece of news with id.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating piece of news with id="
            });
        });
};

exports.updateParticipantsNumber = async(req, res) => {
  
    const filter = { _id: req.params.id };
    const value = req.params.value ? 1 : -1;
    await Event.updateOne(filter, { $inc: {currentParticipantsNumber : value} })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update piece of news with id.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating piece of news with id="
            });
        });
};