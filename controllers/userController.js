const Users = require('../models/UserModel');
const metrics = require('../server')
// Create and Save a new user
exports.createUser = async(req, res) => {
    let user = await Users.findOne({ id: req.body.id});
    if (user) {
      return res.status(400).send('User with the provided id already exist.');
    }
    // Create a user
    user = new Users({
        id: req.body.id,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phoneNumber: req.body.phoneNumber,
        city: req.body.city,
        role: req.body.role,
        enrollments: []
    });
    // Save user in the database
    user
        .save(user)
        .then(data => {
            metrics.totalUsers.inc()
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while creating the user."
            });
        });
};


// Find a single user specifying the id
exports.findUser = async (req, res) => {
    const id = req.params.id
    await Users.findOne({ id: id})
        .then(data => {
            if (!data)
                res.status(404).send({
                    message: "Not found user with id " + id
                });
            else res.send(data);
        })
        .catch(err => {
            console.log(req.params.id)
            res.status(500)
                .send({
                    message: "Error retrieving user with id=" + id
                });
        });
};



