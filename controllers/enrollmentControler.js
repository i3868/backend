const Enrollment  = require('../models/EnrollmentModel')
const metrics = require('../server')
// Create and Save a new enrollment
exports.createEnrollment = (req, res) => {
    // Create an enrollement
    const enrollment = new Enrollment({
        enrolledUserID : req.body.enrolledUserID,
        eventId: req.body.eventId
    });

    // Save the enrollment in the database
    enrollment
        .save(enrollment)
        .then(data => {
            metrics.noEnrollments.inc()
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while creating the post."
            });
        });
};

exports.findUserEnrollment = async (req, res) => {
    const id = req.params.id
    console.log(id)
    await Enrollment.find({enrolledUserID: id})
        .then(data => {
            if (!data)
                res.status(404).send({
                    message: "Not found user with id " + id
                });
            else res.send(data);
        })
        .catch(err => {
            console.log(req.params.id)
            res.status(500)
                .send({
                    message: "Error retrieving user with id=" + id
                });
        });
}

// Delete all enrollments with a specific eventId
exports.deleteAllEnrollmentsEventBased = async (req, res) => {
    const id = req.params.id
    await Enrollment.deleteMany({eventId: id})
        .then(data => {
            if (data) {
                metrics.noEnrollments.dec(data.deletedCount)
                res.send({
                    message: `${data.deletedCount} Post were deleted successfully!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while removing all posts."
            });
        });
};

// Delete one enrollment based on current user id
exports.deleteEnrollment= async (req, res) => {
    const userId = req.params.userId
    const eventId = req.params.eventId
    await Enrollment.deleteOne({enrolledUserID: userId, eventId: eventId})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete enrollment with user id=. Maybe event was not found!`
                });
            } else {
                metrics.noEnrollments.dec()
                res.send({
                    message: "Enrollment was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete enrollment with user id=" 
            });
        });
    };   



