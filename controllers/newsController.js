const News = require('../models/NewsModel')
const metrics = require('../server')
// Retrieve all news from the database.
exports.findAllNews = async (req, res) => {
    await News.find({})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while retrieving events."
            });
        });
};



// Create and Save a new piece of News
exports.createPieceOfNews = async (req, res) => {
    //  Validate request
    if (!req.body.title) {
        res.status(400).send({message: "Title cannot be empty!"});
        return;
    }
    if (!req.body.description) {
        res.status(400).send({message: "Description cannot be empty!"});
        return;
    }
    if (!req.body.content) {
        res.status(400).send({message: "Content cannot be empty!"});
        return;
    }
    let pieceOfNews = await News.findOne({ title : req.body.title});
    if (pieceOfNews) {
      return res.status(400).send('Stirea with the provided name already exist.');
    }
    // Create a piece of news
    pieceOfNews = new News({
        title: req.body.title,
        description: req.body.description,
        content: req.body.content,
    });

    // Save the piece of news in the database
    pieceOfNews
        .save(pieceOfNews)
        .then(data => {
            metrics.noNews.inc()
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while creating the post."
            });
        });
};

// Find a single piece of news with an id
exports.findPieceOfNews = async(req, res) => {
    const {id} = req.params;

    await News.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({
                    message: "Not found piece of news with id " + id
                });
            else res.send(data);
        })
        .catch(err => {
            res.status(500)
                .send({
                    message: "Error retrieving piece of news with id=" + id
                });
        });
};

// Update a piece of news
exports.updatePieceOfNews = async(req, res) => {
  
    const filter = { _id: req.params.id };
    let update = {
        title: req.body.title,
        description: req.body.description,
        content: req.body.content,
    };
      
    await News.updateOne(filter, { $set: update })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update piece of news with id.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating piece of news with id="
            });
        });
};

exports.deleteNews = async (req, res) => {
    const id = req.params.id
    await News.deleteOne({_id: id})
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete piece of news with id=${id}. Maybe event was not found!`
                });
            } else {
                metrics.noNews.dec()
                res.send({
                    message: "Piece of news was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete piece of news with id=" + id
            });
        });
    };   


// Delete all news from the database.
exports.deleteAllNews = async(req, res) => {
    await News.deleteMany({id: req.body.id})
        .then(data => {
            if (data) {
                metrics.noNews.set(0)
                res.send({
                    message: `${data.deletedCount} News were deleted successfully!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message
                    || "Some error occurred while removing all news."
            });
        });
};



