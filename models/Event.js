const mongoose = require('mongoose')

//userSchema
const eventSchema = new mongoose.Schema({
    name: { type: String, required: true},
    eventDate: {type: String, required: true},
    eventLocation: { type: String, required: true},
    maxParticipants: { type: Number, required: true},
    currentParticipantsNumber: { type: Number, required: true},
    purpose:  { type: String, required: true},
    date: { type: Date, default: Date.now}
})

module.exports = mongoose.model('Event', eventSchema)