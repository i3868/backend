const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId;

//userSchema
const userSchema = new mongoose.Schema({
    id: { type: String, required: true},
    email: { type: String, required: true},
    firstName: { type: String, required: true, unique: true},
    lastName: { type: String, required: true},
    phoneNumber: { type: String, required: true},
    city: { type: String, required: true},
    enrollments: {type: [ObjectId], default: [], ref:'Enrollement'},
    role: {type: String, required: true},
    date: { type: Date, default: Date.now}
})

module.exports = mongoose.model('Users', userSchema)