const mongoose = require('mongoose')

const enrollmentSchema = new mongoose.Schema({
    enrolledUserID : {type: String, required: true},
    eventId: { type: String, required: true},
})

enrollmentSchema.method("toJSON", function () {
    const {__v, _id, ...object} = this.toObject();
    object.id = _id;
    return object;
});

module.exports = mongoose.model('Enrollment', enrollmentSchema.method())