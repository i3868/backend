module.exports =  {
    CreateEvents : "create:events",
    UpdateEvents : "update:events",
    DeleteEvents : "delete:events",
    CreateNews : "create:news",
    UpdateNews : "update:news",
    DeleteNews : "delete:news",
  }
